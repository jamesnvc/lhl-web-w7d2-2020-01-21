import React, {useState, useEffect} from 'react';
import './App.css';

const Timer = (props) => {
  const [time, setTime] = useState(0);
  useEffect(() => {
    const interval = setInterval(() => { setTime(oldTime => oldTime + 1); },
                                 1000);
    return () => { console.log("clearing interval");
                   clearInterval(interval); };
  }, []);

  return (<span>Time {time}</span>);
};

const DaysView = (props) => {
  const [days, setDays] = useState([]);
  const [appointments, setAppointments] = useState({});

  useEffect(() => {
    Promise.all([
      fetch("http://localhost:8001/api/days").then(resp => resp.json()),
      fetch("http://localhost:8001/api/appointments").then(resp => resp.json()),
    ])
      .then(([daysData, appointmentsData]) => {
        console.log(daysData);
        console.log(appointmentsData);
        setDays(daysData);
        console.log('set days');
        setAppointments(appointmentsData);
        console.log('set appointments');
        return true;
      });
  }, []);


  const updateAppointment = (apptId, newValue) => {
    // subtly bad way
    const oldAppt = appointments[apptId];
    const newAppt = {...oldAppt, thing: newValue};
    setAppointments(oldAppts => ({...oldAppts, [apptId]: newAppt}));
    // correct way
    setAppointments(oldAppts => {
      const oldAppt = oldAppts[apptId];
      const newAppt = {...oldAppt, thing: newValue};
      return {...oldAppts, [apptId]: newAppt};
    });

    // setAppointments(oldAppts => ({...oldAppts, [apptId]: {id: apptId, thing: newValue}}))
  };

  return (
    <ol>
      {days.map(day => (
        <li key={day.id}>{day.name}
          <ul>
            {day.appointments.map(apptId => {
              let appointment = appointments[apptId];
              console.log("appointment", apptId, appointment);
              if (appointment) {
                return (<li key={apptId}>{appointment.time}:
                          {appointment.interview && appointment.interview.student}</li>);
              } else {
                return <li key={Math.random()}>None</li>;
              }
            })}
          </ul>
        </li>))}
    </ol>
  );
};

function App() {
  const [renderValue, setValue] = useState(0);
  const [stuff, setStuff] = useState([]);
  const [showTimer, setShowTimer] = useState(true);

  useEffect(() => {
    console.log("Render value is ", renderValue);
    document.title = `Value is ${renderValue}`;
    // setValue(v => v + 1); // this causes an infinite loop of rendering
    return () => { console.log("about to re-run effect"); };
  }, [renderValue]);

  return (
    <div className="App">
      <header className="App-header">
        {showTimer && (<Timer/>)}
        <button onClick={(e) => { setShowTimer(show => !show)}}>
          Toggle Timer
        </button>
        <span>Value is {renderValue}</span>
        <button onClick={(e) => {
          setValue(value => value + 1);
          setValue(value => value + 1);
        }}>Increment</button>
        <ul>
          {stuff.map(thing => (<li key={thing}>Thing {thing}</li>))}
        </ul>
        <button onClick={(e) => {
          // stuff.push(Math.random()); // NEVER DO THIS
          // setValue(value => value + 1);

          setStuff(prevStuff => [...prevStuff, Math.random()]);
        }}>Add Thing</button>
        <DaysView/>
      </header>
    </div>
  );
}

export default App;
